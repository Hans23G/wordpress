<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xx2ja&+86(OvM{ aCJYIJi&`#Znxm:6 l>J+v]yKs?lCMPZvhpMLZwsYl5>^+H`X');
define('SECURE_AUTH_KEY',  '.uH)s.[HKT~&w.|b^e1DBRf!vej$ot$-NXhIg86tJPWQ|,DrdM$[n![FJUg<6n>:');
define('LOGGED_IN_KEY',    '$=jj.iL+NGKze_[lt~63+#+QR9lU.-p qxi&n)Ad+8o/6IH,`4iL+wpRASE;K+9a');
define('NONCE_KEY',        'U1NxasZzKm=~SG$LBu3QeT:#yxPJp2%e.M)($FrOfvtk+.*CcT+TPPn_}_f+72,i');
define('AUTH_SALT',        'Aog$w[9NR!iJ2y,%|0A-VGZ)FN[C2./snl4eu=ek7#kZ((w*.=v_}L==HSCCF5nc');
define('SECURE_AUTH_SALT', 'Y;.n|S~VOf^Ml^Ax#By2LA?FOHEuZG+hBKV~`>8)#WR}^t6-*;:GUEHl!;vGWlLS');
define('LOGGED_IN_SALT',   '?5tPo,$dPWf1/1/Z;,*9/I?@1.0%Jg&_qjNt@dh0(gj z#jf!c:LlvGvkY[J{_ u');
define('NONCE_SALT',       'X.cL, A5+^Y]V6-FmvBP`+1#P7x--)>+?@|QM]CP{w||.A4Qu]~yBgaU8epprIDi');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
